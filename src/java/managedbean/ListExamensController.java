/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entities.Examens;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import sessions.ExamensFacade;

/**
 *
 * @author ichraq
 */
@Named(value = "listExamensController")
@SessionScoped
public class ListExamensController implements Serializable {

    /**
     * Creates a new instance of ListExamensController
     */
    public ListExamensController() {
    }
     @EJB
    private ExamensFacade examensFacade;
    
    /**
     * Creates a new instance of ListDesExamens
     */
    
    public List<Examens> getExamens(){
        return examensFacade.findAll();
    }
    public String passer(int idExamen){
        return "passerExamen?idExam=" +idExamen;
    }
    public String edit(int idExamen){
        return "/jsf/examens/List?idExam=" +idExamen;
    }
    
}
