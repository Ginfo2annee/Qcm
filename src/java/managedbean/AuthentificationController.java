/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entities.Etudiants;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import sessions.EtudiantsFacade;

/**
 *
 * @author ichraq
 */
@Named(value = "authentificationController")
@SessionScoped
public class AuthentificationController implements Serializable {
     @EJB
    private EtudiantsFacade etudiantsFacade;

    private Etudiants etudiant;

    public Etudiants getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiants etudiant) {
        this.etudiant = etudiant;
    }

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    private String password;
    /**
     * Creates a new instance of AuthentificationController
     */
    public AuthentificationController() {
    }
   
      public String authentifier() {
	Etudiants e = etudiantsFacade.connect(email, password);
	if (e == null) {
	    System.out.println("Etudiant inexistant");
	    return "login";
	}
	etudiant = e;
	return "ListExamens";
    }
      public String connectProfesseur() {
	etudiant = null;
	return "ListExamens";
    }

    
}
